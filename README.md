# dark_photon



## Installation
Create a conda (mamba, a faster version of conda) environment. Install zfit using pip at first, the rest using conda/mamba
```bash
mamba create -n python38_ana_def python=3.8 
mamba activate python38_ana_def
pip install zfit
mamba install matplotlib mplhep ipykernel scikit-hep pytables awkward-pandas, seaborn, openssl, certifi, ca-certificates -c conda-forge
```
# zfit env has to be created from scratch with zfit installed with pip at first, rest with conda (mamba)
# after that
## Setup
Append this package to the ```PYTHONPATH``` var
```bash
export PYTHONPATH=${PYTHONPATH}:"/path/to/dark_photon"

```

Modify ```data_processor.mumugammas_config``` file to set locations of ```.root``` files and output dirs

## Minimal example

# Processing .root files
```bash
python data_processor/make_dfs.py mumu eta2mumu_test 10  --cut_string 'mass >= 0.25 & mass <= 0.75'
```

# Fit to eta2mumu and compute sWeights
```bash
python analysis_example.py 
```
