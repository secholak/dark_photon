
# root files
raw_f_path = '/eos/user/s/secholak/CMS/eta2mumug/ntuples/ParkingDoubleMuonLowMass*/*/*/*/*.root'
# preprocessed dataframes location
out_dir = '/eos/user/s/secholak/CMS/eta2mumug/ntuples'
# output plots
plots_path = "/afs/cern.ch/work/s/secholak/cms/workdir/eta2mumug/python/tests/eta2mumu_fits"
