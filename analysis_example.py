import pandas as pd
import uproot
import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np
import mplhep as hep
plt.style.use([hep.style.CMS])

import os
from os.path import join
from glob    import glob

## d photon modules
from analysis_config import out_dir, plots_path
from fit_utils.fitter import fit_mumu_2G, eval_sweights
from data_processor.mumugammas_config import tree_name


sel_name = 'eta2mumu_test'
fit_var = 'mass'
n_max = None #load everything, might consume too much RAM if hdf files are too heavy!
# print("Using only 10 df files")
# n_max = 10 #fraction of data
selection_cut = 'probVtx > 0.05'
fit_window = (0.45, 0.65)

proc_dfs_path = f"{out_dir}/{sel_name}/*.h5"
proc_dfs = glob(proc_dfs_path)

## load dfs 
print("Loading processed dataframes: ", proc_dfs_path)
dfs_list = []
for df_file in proc_dfs[:n_max]:
    print("loading:  ", df_file)
    df = pd.read_hdf(df_file, key = tree_name, mode = 'r')
    df.query(selection_cut, inplace = True)
    df['mumu_FD_XY'] = df.eval('sqrt((vtxX-pvX)**2 + (vtxY-pvY)**2 )')
    dfs_list.append(df)

df_merged = pd.concat(dfs_list, copy=False, ignore_index=True)



## do fit 
print("Running fit")
data_np = df_merged[fit_var].to_numpy(copy = True)
data, sum_model, Nsig, Nbkg, fit_result = fit_mumu_2G(data_np, fit_window, key = '_', nbins = 50, binned_fit = True)

## compute sWeights
# sweights can be only evaluated for the fit range events
df_merged.query(f"{fit_var} > {fit_window[0]} & {fit_var} < {fit_window[1]}", inplace = True)
df_weighted =  eval_sweights(df_merged, data, sum_model, Nsig, Nbkg)


# dummy plot
bins = np.linspace(6.5,30, 20)
normalisation  = True
color_alpha = 0.3
hist_vals, hist_bins, _ = plt.hist( df_weighted.pt, 
                                    bins = bins, 
                                    weights=df_weighted['weights_Nsig'],
                                    density= normalisation, 
                                    label = r'$\eta \to \mu\mu$',
                                    alpha = color_alpha,)

plt.legend()
plt.xlabel(r'$p_T$')
plts_out = plots_path + "/eta_pt"
if not os.path.exists(plts_out):
    os.makedirs(plts_out)
plt.savefig(f'{plts_out}/eta2mumu_pt_sweights.pdf', dpi = 200)
