import pandas as pd
import uproot
import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np
import os
from os.path import join
from glob    import glob
import vector


# d_photon modules
from data_processor.mumugammas_maker import make_mumugamma_df, make_dimudigamma_dfs, make_mumugamma_best_dRpT_df
# constants
from data_processor.mumugammas_config import ETA_MASS, OMEGA_MASS, MU_MASS, PI0_MASS
# lists of branches 
from data_processor.mumugammas_config import branches_mumu, branches_muIDs, branches_dimudigamma, branches_mumugamma, branches_nested, branches_gamma, tree_name

# braches that are present in .root files
branches = branches_mumu + branches_gamma +branches_nested


from analysis_config import raw_f_path, out_dir


def save_df(dfs_list, name, df_dir, i_df):
        df = pd.concat(dfs_list, copy=False, ignore_index=True)
        df_name = df_dir + f'/df_{name}_{i_df}.h5'

        print(f'saving {df_name} into \n', df_name)
        df.to_hdf(df_name, key=tree_name, mode='w')

def unfold_muID(df):

    # check if muID columns are there
    if "muonID1" in df.columns:

        # unfold arrays of muonID vars into separate columns
        df = df.join(pd.DataFrame(df.muonID1.tolist(),index=df.index).add_prefix('muonID1_'))
        df = df.join(pd.DataFrame(df.muonID2.tolist(),index=df.index).add_prefix('muonID2_'))

        # rename to give more comprehensive names
        muID_columns = ['isHighPtMuon', 'isLooseMuon', 'isMediumMuon', 'isSoftMuon', 'isTightMuon']
        mu1ID_names = { f'muonID1_{i_col}' : f'muon1_{col_name}' for i_col, col_name in enumerate(muID_columns)}
        mu2ID_names = { f'muonID2_{i_col}' : f'muon2_{col_name}' for i_col, col_name in enumerate(muID_columns)}
        df.rename(columns=mu1ID_names, inplace=True)
        df.rename(columns=mu2ID_names, inplace=True)

        df = df.drop('muonID1', axis=1)
        df = df.drop('muonID2', axis=1)

    return df


def drop_array_cols(df):
    drop_list = branches_gamma + ['l1Result']
    df = df.drop(drop_list, axis=1)

    return df


def make_mumugamma(n_start = 0, n_stop = -1, sel_name = 'mumugamma_best_dR', cut_string = 'n_pfPhotons > 0 & mass < 1 & probVtx > 0.05'):
    #  preprocess ntuples with mumu vertexes, combine dimugamma/dimudigamma
    
    raw_files = glob(raw_f_path)

    
    df_dir = f"{out_dir}/{sel_name}"
    if not os.path.exists(df_dir):
        os.makedirs(df_dir)

    mumugamma_dfs = []

    i_merged = int(n_start / 10)
    for i_file, f in enumerate(raw_files[n_start:n_stop]): 

        # open tree and apply a basic preselection
        with uproot.open(f) as tree:
            df = tree[tree_name].arrays(branches, library="pd", how = None)
        # eval number of pfPhotons in each evt (corresponding to mumu Parked cand)
        df['n_pfPhotons']=df['pfCandPhotonDr'].apply(len)
        df.query(cut_string, inplace = True)
        df = unfold_muID(df)
        # mumugamma_df = make_mumugamma_df(df)
        mumugamma_df = make_mumugamma_best_dRpT_df(df)
        mumugamma_dfs.append(mumugamma_df)

        if i_file%10 == 0 and i_file != 0:
            print(i_file)
            print('saving  ', len(mumugamma_dfs), '  files')
            save_df(mumugamma_dfs, sel_name, df_dir, i_merged)

            mumugamma_dfs = []
            i_merged+=1

    # saving remaining dfs
    print('saving  ', len(mumugamma_dfs), '  files')
    save_df(mumugamma_dfs, sel_name, df_dir, i_merged)


def make_mumu(n_start = 0, n_stop = -1, sel_name = 'phi2mumu', cut_string = 'mass >= 0.25 & mass <= 0.42'):
    #  preprocess ntuples with mumu vertexes, select a mass window
    
    print("Loading .root files from:  ", raw_f_path)
    raw_files = glob(raw_f_path)
    df_dir = f"{out_dir}/{sel_name}"
    if not os.path.exists(df_dir):
        os.makedirs(df_dir)

    mumu_dfs = []

    i_merged = int(n_start / 10)
    for i_file, f in enumerate(raw_files[n_start:n_stop]): 

        # open tree and apply a basic preselection
        with uproot.open(f) as tree:
            df = tree[tree_name].arrays(branches, library="pd", how = None)
        df.query(cut_string, inplace = True)
        df = unfold_muID(df)
        df = drop_array_cols(df)
        mumu_dfs.append(df)
        if i_file%10 == 0 and i_file != 0:
            print(i_file)
            print('saving  ', len(mumu_dfs), '  files')
            save_df(mumu_dfs, sel_name, df_dir, i_merged)

            mumugamma_dfs = []
            i_merged+=1

    # saving remaining dfs
    print('saving  ', len(mumu_dfs), '  files')
    save_df(mumu_dfs, sel_name, df_dir, i_merged)


def make_mumugammagamma(n_start = 0, n_stop = -1, sel_name = 'mumugamma_best_dR', cut_string = 'n_pfPhotons > 0 & mass < 1 & probVtx > 0.05'):
    print("Not implemented yet")



processors = {
    'mumu'           : make_mumu,
    'mumugamma'      : make_mumugamma,
    'mumugammagamma' : make_mumugammagamma,
}




import sys
if __name__ == '__main__' :

    import argparse

    parser = argparse.ArgumentParser(description="Run over ntuples containing mumu vtx + photons and make dfs")

    parser.add_argument('combination', 
                        choices = ['mumu', 'mumugamma', 'mumugammagamma'],
                        default = 'mumu',
                        help='particles combination to produce')
    parser.add_argument('sel_name', 
                        default = 'eta2mumu',
                        help='Dir to save in')
    parser.add_argument('n_files',
                        type = int, 
                        default = 100,
                        help='Number of .root files to process. Each new df file will contain 10 .root files')

    parser.add_argument('--cut_string', 
                        type=str,
                        default = 'mass >= 0.25 & mass <= 0.75',
                        help='Dir to save in')
    


    args = parser.parse_args()

    combination       = args.combination
    sel_name          = args.sel_name
    cut_string        = args.cut_string
    n_files           = args.n_files

    processors[combination](
        n_start = 0, 
        n_stop = n_files, 
        sel_name = sel_name, 
        cut_string = cut_string
        )
