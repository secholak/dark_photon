import os
from os.path import join
from glob import glob

dirPath = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/"
savePath = '/eos/user/s/secholak/CMS/eta2mumug/ntuples/inclusiveDiLeptonMC/full/merged/'
ntupleName = 'mmgTree*'


nDone = 0
nFilesInRoot = 50

print("Saving at: " +savePath)
filesList = glob( dirPath+ ntupleName)
print("Merging {0} files".format(len(filesList)))
print("Merging by {0} files in root file".format(nFilesInRoot))
# print(filesList)
#os.system('mkdir '+sjDir)
i = 0
subList = []
for _file in filesList:
    subList.append(_file)
    if len(subList)>=nFilesInRoot:
            filesListString = ' '.join( [ '%s' % f for f in subList ] )
            os.system( f'hadd -fk {savePath}merged_{i}.root {filesListString}')
            print("/00/"*40 + f"    Saving file # {i} ")
            i+=1
            subList=[]
if (len(subList)<nFilesInRoot and len(subList) !=0):
    filesListString = ' '.join( [ '%s' % f for f in subList ] )
    os.system( f'hadd -fk {savePath}merged_{i}.root {filesListString}')
    print("/00/"*40 + f"    Saving file # {i} ")
nDone+=1
    
