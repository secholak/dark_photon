ETA_MASS    = 0.547862
OMEGA_MASS  = 0.78266
MU_MASS     = 0.105658
PI0_MASS    = 0.134977

tree_name = 'tree/tree'

branches_mumu = [
    'eventNum',
    'lumiSec',
    'runNum',
    'mass',
    'pt',
    'dr',
    'pt1',
    'pt2',
    'eta1',
    'eta2',
    'phi1',
    'phi2',
    'pfIso1',
    'pfIso2',
    'dxy1',
    'dxy2',
    'dz1',
    'dz2',
    'trkChi21',
    'trkChi22',
    'trkNdof1',
    'trkNdof2',
    'probVtx',
    'vtxX',
    'vtxY',
    'vtxZ',
    'vtxXError',
    'vtxYError',
    'vtxZError',
    'vtx_chi2',
    'npv',
    'pvX',
    'pvY',
    'pvZ',
    'nPhotons',
]

branches_muIDs = [
    'muon1_isHighPtMuon',
    'muon1_isLooseMuon',
    'muon1_isMediumMuon',
    'muon1_isSoftMuon',
    'muon1_isTightMuon',
    'muon2_isHighPtMuon',
    'muon2_isLooseMuon',
    'muon2_isMediumMuon',
    'muon2_isSoftMuon',
    'muon2_isTightMuon',
]

branches_nested= [
 'muonID1',
 'muonID2',
 'l1Result',
#  'hltResult',
]
branches_gamma = [
#  'slimmedPhotonPt',
#  'slimmedPhotonEta',
#  'slimmedPhotonPhi',
#  'slimmedPhotonM',
#  'slimmedPhotonSigmaIetaIeta',
#  'slimmedPhotonHOverE',
#  'slimmedPhotonEcalIso',
#  'slimmedPhotonHcalIso',
#  'slimmedPhotonTrkIso',
#  'slimmedPhotonR9',
 'pfCandPhotonDr',
 'pfCandPhotonIso',
 'pfCandPhotonPt',
 'pfCandPhotonEta',
 'pfCandPhotonPhi',
 'pfCandPhotonEnergy',
 'pfCandPhotonEt',
 'pfCandPhotonEt2'
 ]


branches_dimudigamma = [
    'digamma_mass',
    'dimudigamma_mass',
    'dimudigamma_pt',
    'dimudigamma_dr',
    'dimudigamma_phi',
    'dimudigamma_eta',
    'dr_dimugamma1',
    'dr_dimugamma2',
    'digamma_dr',
    'digamma_pt',
    'digamma_phi',
    'digamma_eta',
    'gamma1_pt',
    'gamma1_eta',
    'gamma1_phi',
    'gamma1_iso',
    'gamma2_pt',
    'gamma2_eta',
    'gamma2_phi',
    'gamma2_iso',

    'gamma1_Energy',
    'gamma1_Et',
    'gamma1_Et2',
    'gamma2_Energy',
    'gamma2_Et',
    'gamma2_Et2',
]

branches_mumugamma = [
    'mumugamma_mass',
    'mumugamma_pt',
    'mumugamma_dr',
    'mumugamma_phi',
    'mumugamma_eta',
    'gamma_pt',
    'gamma_eta',
    'gamma_phi',
    'gamma_iso',
    'gamma_Energy',
    'gamma_Et',
    'gamma_Et2',
]

