# reconstruct mumugamma and mumupi0(pi0->gammagamma)
import pandas as pd
import vector
from math import sqrt
import itertools as it

# constants
from mumugammas_config import ETA_MASS, OMEGA_MASS, MU_MASS, PI0_MASS
# lists of branches 
from mumugammas_config import branches_mumu, branches_muIDs, branches_dimudigamma, branches_mumugamma

def eval_dr(vec_1, vec_2):

    return sqrt(vec_1.deltaeta(vec_2)**2 + vec_1.deltaphi(vec_2)**2)

verbose = False

def init_branches(branches):
    return { br:[] for br in branches }

def push_dimu_info(row, dimu_dict):

    for key in dimu_dict:
        dimu_dict[key].append(row[key])
    
    return dimu_dict



def make_mumugamma_df(df, add_branches = []):

    dimu_cands = init_branches(branches_mumu + ['n_pfPhotons'] + branches_muIDs + add_branches)
    mumugamma_cands = init_branches(branches_mumugamma)

    #  loop over mumu vtx and sellect additional photons
    # for row in df.itertuples(index=True, name='Pandas'):
    for i_row in range(len(df)):
        row = df.iloc[i_row]

        if verbose: print("<<<<<<<<< new event")
        mu1 = vector.obj(pt=row.pt1, phi=row.phi1, eta=row.eta1, M=MU_MASS)
        mu2 = vector.obj(pt=row.pt2, phi=row.phi2, eta=row.eta2, M=MU_MASS)
        dimu = mu1 + mu2

        n_comb = 0
        dr_mumugamma_best = 100
        min_dr = 100
        mumugamma_best = None
        gamma_best = None
        gamma_id = None

        # find best photon in pfCandPhotons container
        for i_ph, dr in  enumerate(row.pfCandPhotonDr):
            
            gamma = vector.obj(pt=row.pfCandPhotonPt[i_ph], phi=row.pfCandPhotonPhi[i_ph], eta=row.pfCandPhotonEta[i_ph], M=0.0)
            mumugama = dimu + gamma
            m_mumugama = mumugama.M
            #  in eta mass window?
            if m_mumugama < 0.3 or m_mumugama > 0.8: continue
            # best candidate by min dR
            if dr < min_dr: 
                min_dr = dr
                dr_mumugamma_best = dr
                mumugamma_best  = mumugama
                gamma_best = gamma
                gamma_id   = i_ph   
            
            n_comb+=1

        if mumugamma_best is not None:
            # push computed info
            # gamma 1 info
            mumugamma_cands['gamma_pt'].append(gamma_best.pt)
            mumugamma_cands['gamma_eta'].append(gamma_best.eta)
            mumugamma_cands['gamma_phi'].append(gamma_best.phi)
            mumugamma_cands['gamma_iso'].append(row.pfCandPhotonIso[gamma_id])
            mumugamma_cands['gamma_Energy'].append(row.pfCandPhotonEnergy[gamma_id])
            mumugamma_cands['gamma_Et'].append(row.pfCandPhotonEt[gamma_id])
            mumugamma_cands['gamma_Et2'].append(row.pfCandPhotonEt2[gamma_id])
            
            

            # omega cand info
            mumugamma_cands['mumugamma_mass'].append(mumugamma_best.M)
            mumugamma_cands['mumugamma_pt'].append(mumugamma_best.pt)
            mumugamma_cands['mumugamma_dr'].append(dr_mumugamma_best)
            mumugamma_cands['mumugamma_phi'].append(mumugamma_best.phi)
            mumugamma_cands['mumugamma_eta'].append(mumugamma_best.eta)
            
            # push event info (from vtx candidates)
            dimu_cands = push_dimu_info(row, dimu_cands)           
        if verbose: print(f"Done {n_comb} combinations for {row.n_pfPhotons} pfPhotons")
    # merge two dicts before making a df
    cand_dict = {**dimu_cands, **mumugamma_cands}
    df_mumugamma = pd.DataFrame.from_dict(cand_dict)

    return df_mumugamma



def make_mumugamma_best_dRpT_df(df):

    dimu_cands = init_branches(branches_mumu + ['n_pfPhotons'] + branches_muIDs)
    mumugamma_cands = init_branches(branches_mumugamma)

    #  loop over mumu vtx and sellect additional photons
    # for row in df.itertuples(index=True, name='Pandas'):
    for i_row in range(len(df)):
        row = df.iloc[i_row]

        if verbose: print("<<<<<<<<< new event")
        mu1 = vector.obj(pt=row.pt1, phi=row.phi1, eta=row.eta1, M=MU_MASS)
        mu2 = vector.obj(pt=row.pt2, phi=row.phi2, eta=row.eta2, M=MU_MASS)
        dimu = mu1 + mu2

        n_comb = 0
        dr_mumugamma_best = 100
        min_dRpT = 100
        mumugamma_best = None
        gamma_best = None
        gamma_id = None

        # find best photon in pfCandPhotons container
        for i_ph, dr in  enumerate(row.pfCandPhotonDr):
            
            gamma = vector.obj(pt=row.pfCandPhotonPt[i_ph], phi=row.pfCandPhotonPhi[i_ph], eta=row.pfCandPhotonEta[i_ph], M=0.0)
            mumugama = dimu + gamma
            m_mumugama = mumugama.M
            #  in eta mass window?
            if m_mumugama < 0.3 or m_mumugama > 0.8: continue
            # best candidate by min dR x pT
            mumugama_pt = mumugama.pt
            dRpT = dr * mumugama_pt
            if dRpT < min_dRpT: 
                min_dRpT = dRpT
                dr_mumugamma_best = dr
                mumugamma_best  = mumugama
                gamma_best = gamma
                gamma_id   = i_ph   
            
            n_comb+=1

        if mumugamma_best is not None:
            # push computed info
            # gamma 1 info
            mumugamma_cands['gamma_pt'].append(gamma_best.pt)
            mumugamma_cands['gamma_eta'].append(gamma_best.eta)
            mumugamma_cands['gamma_phi'].append(gamma_best.phi)
            mumugamma_cands['gamma_iso'].append(row.pfCandPhotonIso[gamma_id])
            mumugamma_cands['gamma_Energy'].append(row.pfCandPhotonEnergy[gamma_id])
            mumugamma_cands['gamma_Et'].append(row.pfCandPhotonEt[gamma_id])
            mumugamma_cands['gamma_Et2'].append(row.pfCandPhotonEt2[gamma_id])
            
            

            # omega cand info
            mumugamma_cands['mumugamma_mass'].append(mumugamma_best.M)
            mumugamma_cands['mumugamma_pt'].append(mumugamma_best.pt)
            mumugamma_cands['mumugamma_dr'].append(dr_mumugamma_best)
            mumugamma_cands['mumugamma_phi'].append(mumugamma_best.phi)
            mumugamma_cands['mumugamma_eta'].append(mumugamma_best.eta)
            
            # push event info (from vtx candidates)
            dimu_cands = push_dimu_info(row, dimu_cands)           
        if verbose: print(f"Done {n_comb} combinations for {row.n_pfPhotons} pfPhotons")
    # merge two dicts before making a df
    cand_dict = {**dimu_cands, **mumugamma_cands}
    df_mumugamma = pd.DataFrame.from_dict(cand_dict)

    return df_mumugamma



def make_dimudigamma_dfs(df):

    dimu_cands = init_branches(branches_mumu + ['n_pfPhotons'] + branches_muIDs)
    dimudigamma_cands = init_branches(branches_dimudigamma)

    #  loop over mumu vtx and sellect additional photons
    # for row in df.itertuples(index=True, name='Pandas'):
    for i_row in range(len(df)):
        row = df.iloc[i_row]

        if verbose: print("<<<<<<<<< new event")
        mu1 = vector.obj(pt=row.pt1, phi=row.phi1, eta=row.eta1, M=MU_MASS)
        mu2 = vector.obj(pt=row.pt2, phi=row.phi2, eta=row.eta2, M=MU_MASS)
        dimu = mu1 + mu2

        n_comb = 0
        dr_dimudigamma_best = 100
        min_dm = 100
        digamma_best = None
        dimudigamma_best = None
        gamma_1_best = None
        gamma_2_best = None
        gamma_1_id = None
        gamma_2_id = None

        # permutate indeces of photons, unique ones, wo repetition
        for i_ph, j_ph in it.combinations(range(row.n_pfPhotons), 2):
            
            gamma_1 = vector.obj(pt=row.pfCandPhotonPt[i_ph], phi=row.pfCandPhotonPhi[i_ph], eta=row.pfCandPhotonEta[i_ph], M=0.0)
            gamma_2 = vector.obj(pt=row.pfCandPhotonPt[j_ph], phi=row.pfCandPhotonPhi[j_ph], eta=row.pfCandPhotonEta[j_ph], M=0.0)

            digamma = gamma_1 + gamma_2
            m_digamma = digamma.M
            #  is pi0? (~ +/- 3sigma window to contain 99% of pi0s)
            if m_digamma < 0.09 or m_digamma > 0.17: continue

            dimudigamma = dimu + digamma
            m_dimudigamma = dimudigamma.M
            # dm = abs(m_digamma - PI0_MASS)
            dm = abs(m_dimudigamma - OMEGA_MASS)
            #  in omega mass window?
            if m_dimudigamma < 0.4 or m_dimudigamma > 1: continue

            if dm < min_dm: 
                dr_dimudigamma_best = eval_dr(dimu, digamma)
                min_dm = dm
                digamma_best = digamma
                dimudigamma_best  = dimudigamma
                gamma_1_best = gamma_1
                gamma_2_best = gamma_2
                gamma_1_id   = i_ph   
                gamma_2_id   = j_ph
            
            n_comb+=1
            if verbose: print("gamma_1  ", i_ph, '\n', gamma_1, '\n', "gamma_2  ", j_ph, '\n', gamma_2, '\n', "dimudigamma  ",  dimudigamma)

        if dimudigamma_best is not None:
            # push computed info
            dimudigamma_cands['digamma_dr'].append(eval_dr(gamma_1_best, gamma_2_best))
            dimudigamma_cands['digamma_pt'].append(digamma_best.pt)
            dimudigamma_cands['digamma_phi'].append(digamma_best.phi)
            dimudigamma_cands['digamma_eta'].append(digamma_best.eta)
            dimudigamma_cands['dr_dimugamma1'].append(eval_dr(dimu, gamma_1_best))
            dimudigamma_cands['dr_dimugamma2'].append(eval_dr(dimu, gamma_2_best))
            # gamma 1 info
            dimudigamma_cands['gamma1_pt'].append(gamma_1_best.pt)
            dimudigamma_cands['gamma1_eta'].append(gamma_1_best.eta)
            dimudigamma_cands['gamma1_phi'].append(gamma_1_best.phi)
            dimudigamma_cands['gamma1_iso'].append(row.pfCandPhotonIso[gamma_1_id])
            dimudigamma_cands['gamma1_Energy'].append(row.pfCandPhotonEnergy[gamma_1_id])
            dimudigamma_cands['gamma1_Et'].append(row.pfCandPhotonEt[gamma_1_id])
            dimudigamma_cands['gamma1_Et2'].append(row.pfCandPhotonEt2[gamma_1_id])

            # gamma 2 info
            dimudigamma_cands['gamma2_pt'].append(gamma_2_best.pt)
            dimudigamma_cands['gamma2_eta'].append(gamma_2_best.eta)
            dimudigamma_cands['gamma2_phi'].append(gamma_2_best.phi)
            dimudigamma_cands['gamma2_iso'].append(row.pfCandPhotonIso[gamma_2_id])
            dimudigamma_cands['gamma2_Energy'].append(row.pfCandPhotonEnergy[gamma_2_id])
            dimudigamma_cands['gamma2_Et'].append(row.pfCandPhotonEt[gamma_2_id])
            dimudigamma_cands['gamma2_Et2'].append(row.pfCandPhotonEt2[gamma_2_id])

            # omega cand info
            dimudigamma_cands['digamma_mass'].append(digamma_best.M)
            dimudigamma_cands['dimudigamma_mass'].append(dimudigamma_best.M)
            dimudigamma_cands['dimudigamma_pt'].append(dimudigamma_best.pt)
            dimudigamma_cands['dimudigamma_dr'].append(dr_dimudigamma_best)
            dimudigamma_cands['dimudigamma_phi'].append(dimudigamma_best.phi)
            dimudigamma_cands['dimudigamma_eta'].append(dimudigamma_best.eta)
            
            # push event info (from vtx candidates)
            dimu_cands = push_dimu_info(row, dimu_cands)           
        if verbose: print(f"Done {n_comb} combinations for {row.n_pfPhotons} pfPhotons")
    # merge two dicts before making a df
    cand_dict = {**dimu_cands, **dimudigamma_cands}
    df_dimudigamma = pd.DataFrame.from_dict(cand_dict)

    return df_dimudigamma



