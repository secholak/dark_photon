import zfit
from zfit import z as ztf
import tensorflow as tf




from math import pi
class ThreshPol(zfit.pdf.ZPDF):
    _N_OBS = 1  # dimension, can be omitted
    _PARAMS = ['x0', 'alpha', 'a0', 'a1']  # the name of the parameters

    def _unnormalized_pdf(self, x):
        x = ztf.unstack_x(x)  # returns a list with the columns: do x, y, z = z.unstack_x(x) for 3D
        x0 = self.params['x0']
        a0 = self.params['a0']
        a1 = self.params['a1']
        alpha = self.params['alpha']
        return (x - x0)**alpha * (a0 + a1*x)       


def thresh_pol_integral(limits, params, model):
    x0 = params['x0']
    a0 = params['a0']
    a1 = params['a1']
    
    (lower,), (upper,) = limits.limits

    zmax, _ = model._z(upper)
    zmin, _ = model._z(lower)
    rmax = -nu + tf.asinh(zmax) / tau
    rmin = -nu + tf.asinh(zmin) / tau
    
    sqrt2 = tf.sqrt(ztf.to_real(2.))

    return  0.5 * (tf.math.erf(rmax / sqrt2) - tf.math.erf(rmin / sqrt2))


class JohnsonSU(zfit.pdf.ZPDF):
    _N_OBS = 1  # dimension, can be omitted
    _PARAMS = ["mu", "sigma", "nu", "tau"]  # the name of the parameters
    
    def _unnormalized_pdf(self, x):
        x = ztf.unstack_x(x)
        nu = self.params['nu']
        tau = self.params['tau']

        z, _lambda = self._z(x)
        r = -nu + tf.asinh(z) / tau

        ret = 1. / (tau * _lambda * ztf.sqrt(2. * pi))
        ret *= 1. / tf.sqrt(z * z + 1.)
        ret *= ztf.exp(-0.5 * r * r)

        return ret
    
    def _z(self, x):
        mu = self.params['mu']
        sigma = self.params['sigma']
        nu = self.params['nu']
        tau = self.params['tau']
        
        w = ztf.exp(tau * tau)
        omega = - nu * tau
        c = 0.5 * (w-1) * (w * tf.cosh(2. * omega) + 1.)
        c = tf.pow(c, -0.5)

        _lambda = sigma * c
        xi = mu + _lambda * tf.sqrt(w) * tf.sinh(omega)
        z = (x - xi) / _lambda
        
        return z, _lambda
        

def jsu_integral(limits, params, model):
    nu = params['nu']
    tau = params['tau']
    
    (lower,), (upper,) = limits.limits

    zmax, _ = model._z(upper)
    zmin, _ = model._z(lower)
    rmax = -nu + tf.asinh(zmax) / tau
    rmin = -nu + tf.asinh(zmin) / tau
    
    sqrt2 = tf.sqrt(ztf.to_real(2.))

    return  0.5 * (tf.math.erf(rmax / sqrt2) - tf.math.erf(rmin / sqrt2))


limits = zfit.Space(axes=0, limits=(zfit.Space.ANY_LOWER, zfit.Space.ANY_UPPER))
JohnsonSU.register_analytic_integral(func=jsu_integral, limits=limits)
