import pandas as pd
import uproot
import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np
import mplhep as hep
plt.style.use([hep.style.CMS])

import os
from os.path import join
from glob    import glob

import zfit
from hepstats.splot import compute_sweights

from fit_utils.plot_utils import plotFit
from fit_utils.custom_pdfs import ThreshPol, JohnsonSU
from analysis_config import plots_path



def run_minimisation(sum_model, data, nbins, fit_window, binned_fit):
    minimizer = zfit.minimize.Minuit(gradient=False, mode = 1)

    if binned_fit:
        # make binned
        binning = zfit.binned.RegularBinning(nbins, *fit_window, name="mass_1")
        obs_bin = zfit.Space("mass_1", binning=binning)

        data_binned = data.to_binned(obs_bin)
        model_binned = zfit.pdf.BinnedFromUnbinnedPDF(sum_model, obs_bin)
        nll = zfit.loss.ExtendedBinnedNLL(model_binned, data_binned)
    else:
        nll = zfit.loss.ExtendedUnbinnedNLL(sum_model, data)

    nll = zfit.loss.ExtendedUnbinnedNLL(sum_model, data)
    result = minimizer.minimize(nll)
    
    if not result.converged or not result.valid:
            for i_fit in range(10):
                print(f'Fit # {i_fit}')
                result = minimizer.minimize(nll)
                if result.converged and result.valid:
                    break
                if i_fit%3 == 0:
                    print('Params are randomised!')
                    params = nll.get_params()
                    for param in params:
                        if isinstance(param, zfit.param.Parameter):
                            param.randomize()
                # print('Didn`t converge, fitting one more time')    
            print(result)

    print(result.info['minuit'])
    result.hesse(name='hesse_np')

    return result




def fit_mumu_2G(data_np, fit_window, key = '_', nbins = 50, binned_fit = True):

    zfit.run.clear_graph_cache()
    obs_1 = zfit.Space('mass_1', limits=fit_window)
    data = zfit.data.Data.from_numpy(obs=obs_1, array=data_np)

   
    n_largest =  1.5*len(data_np)
    n_sig_init = len(data_np)/5
    n_bkg_init = len(data_np)/2
    ## in case params are being recreated multiple times (zfit / tf requirement to have a unique name for each declared param)
    ll = np.random.uniform(0,1e3)

    #### fit params
    ## pdfs normalisation
    Nbkg = zfit.Parameter(f"Nbkg_{ll}", n_bkg_init, 0., n_largest, step_size=1)
    Nsig = zfit.Parameter(f"Nsig_{ll}", n_sig_init, 1000, n_largest, step_size=1)

    
    ## shape parameters
    mu_sig_1 = zfit.Parameter(f'mu_sig_1_{ll}', 0.547, 0.54, 0.56, step_size=0.01)
    ## gaus pdfs fraction
    gauss_fraction =  zfit.Parameter(f"gaus_frac_{ll}", 5.8e-1, 0.01, 1, step_size=0.01)
    sigma_sig_1 = zfit.Parameter(f'sigma_sig_1_{ll}',   7.8e-3, 3.0e-3, 1.0e-2, step_size=0.1)
    sigma_sig_2 = zfit.Parameter(f'sigma_sig_2_{ll}',   4.2e-3, 1.0e-3, 9.0e-3, step_size=0.1)

    

    # bkg_pdf_1 = zfit.pdf.Exponential(obs=obs_1,lam = slope_expo_1)
    slope_cheb = zfit.Parameter(f"slope_cheb_{ll}", -0.1, -1., 1., step_size=0.001) 
    bkg_pdf_1 = zfit.pdf.Chebyshev(obs =obs_1, coeffs = [slope_cheb])

    bkg_pdf_ext_1 = bkg_pdf_1.create_extended(Nbkg)

    sig_pdf_1 = zfit.pdf.Gauss(obs=obs_1, mu=mu_sig_1, sigma=sigma_sig_1)
    sig_pdf_2 = zfit.pdf.Gauss(obs=obs_1, mu=mu_sig_1, sigma=sigma_sig_2)

    sig_pdf_sum = zfit.pdf.SumPDF([sig_pdf_1, sig_pdf_2], fracs= [gauss_fraction])
    sig_pdf_sum_ext = sig_pdf_sum.create_extended(Nsig)

    sum_models_list = [bkg_pdf_ext_1,sig_pdf_sum_ext]
    sum_model  = zfit.pdf.SumPDF(sum_models_list)

    fit_result = run_minimisation(sum_model, data, nbins, fit_window, binned_fit)


    nsig = fit_result.params[Nsig]['value']
    errors, _ =  fit_result.errors()
    nsig_err = fit_result.params[Nsig]['hesse']['error']

    plotsList = {
            "data":{
                "label": "Data",
                "color": "black"
            },
            "fullmodel": {
                "label": "Full fit", 
                "color":"red",
            },
            "model_0": {
                "label": f"Cheb (1st), Nbkg = {fit_result.params[Nbkg]['value']:.1f}", 
                "color":"blue",
            },
            "model_1": {
                "label": f"Double Gaussian, N = {nsig:.1f}+/-{nsig_err:.1f}\n", 
                "color":"green",
            },
        }

    fig, ax1, ax2 = plotFit(sum_model, data_np, plotsList, x_label="$\mu\\mu$ mass [GeV]", nbins=100, linewidth=0.1, rangeX = fit_window, plot_chi2 = True,
        logy = False)

    plt.legend(loc = 'best', fontsize = 'x-large')
    if not os.path.exists(plots_path):
        os.makedirs(plots_path)
    plt.savefig(f'{plots_path}/eta2mumu_massFit_{key}.pdf', dpi = 200)
    plt.close()


    return data, sum_model, Nsig, Nbkg, fit_result


def fit_mumugamma(data_np, fit_window, key = '_', nbins = 50, binned_fit = True, with_bkg = True, sig_pdf = 'JohnsonSU'):
    zfit.run.clear_graph_cache()

    # obs and data
    n_largest =  1.5*len(data_np)
    n_sig_init = len(data_np)/5
    n_bkg_init = len(data_np)/2
    obs_1 = zfit.Space('mass_1', limits=fit_window)
    data = zfit.data.Data.from_numpy(obs=obs_1, array=data_np)


    ## in case params are being recreated multiple times:
    ll = np.random.uniform(0,1e3)

    ## pdfs normalisation
    Nbkg = zfit.Parameter(f"Nbkg_{ll}", n_bkg_init, 0., n_largest, step_size=1)
    Nsig = zfit.Parameter(f"Nsig_{ll}", n_sig_init, 1000, n_largest, step_size=1)

    ########## sig pdfs and params
    ## Johnson SU params
    mu_sig = zfit.Parameter(f'mu_sig_1_{ll}', 0.548, 0.5, 0.6, step_size=0.01)
    if sig_pdf == 'JohnsonSU':
        sigma_jh = zfit.Parameter(f'sigma_jh_{ll}',   0.0259644, 1.0e-3, 5e-2, step_size=0.1)
        nu_jh = zfit.Parameter(f'nu_{ll}', -53.7961 , -100 , 0, step_size=0.1)
        tau_jh = zfit.Parameter(f'tau_{ll}', 0.126528 , 0, 1, step_size=0.1)
        sig_pdf_2 = JohnsonSU(obs=obs_1, mu=mu_sig, sigma=sigma_jh, nu = nu_jh, tau = tau_jh)
    
    elif sig_pdf == 'CB':
        ## cb params
        alpha_cb = zfit.Parameter(f'alpha_cb_{ll}', 1.4 , 0, 10, step_size=0.1)
        # alpha_cb = zfit.Parameter(f'alpha_cb_{ll}', -0.5 , -10, 0, step_size=0.1)
        n_cb = zfit.Parameter(f'n_cb_{ll}', 1 , 0, 10, step_size=0.1)
        sigma_cb = zfit.Parameter(f'sigma_cb_{ll}',   3e-3, 1.0e-3, 1.2e-2, step_size=0.1)
        sig_pdf_2 = zfit.pdf.CrystalBall(obs=obs_1, mu=mu_sig, sigma=sigma_cb, n = n_cb, alpha = alpha_cb)
    sig_pdf_2_ext = sig_pdf_2.create_extended(Nsig)

    ## bkg pdf    
    x0 = zfit.Parameter(f"x0_{ll}", 0.2, floating = False)
    a0 = zfit.Parameter(f"a0_{ll}", 5.60905, 1., 10., step_size=0.1)
    a1 = zfit.Parameter(f"a1_{ll}", -5.28406, -10., 10., step_size=0.1)
    alpha = zfit.Parameter(f"alpha_{ll}", 2.5, .1, 5., step_size=0.1)

    bkg_pdf = ThreshPol( obs=obs_1, x0 = x0 , a0 = a0 , a1 = a1 , alpha = alpha )
    # bkg_pdf = bkg_pdf.to_binned(obs_bin)
    bkg_pdf_ext = bkg_pdf.create_extended(Nbkg)

    sum_models_list = [sig_pdf_2_ext]
    if with_bkg:
        sum_models_list.append(bkg_pdf_ext)

    if len(sum_models_list) > 1:
        sum_model  = zfit.pdf.SumPDF(sum_models_list)
    else: sum_model = sum_models_list[0]

    result = run_minimisation(sum_model, data, nbins, fit_window, binned_fit)


    nsig = result.params[Nsig]['value']    #
    # errors, _ =  result.errors()
    result.hesse(name='hesse')
    nsig_err = result.params[Nsig]['hesse']['error']

    plotsList = {
            "data":{
                "label": "Data",
                "color": "black"
            },
            "fullmodel": {
                "label": "Full fit", 
                "color":"red",
            },
            
            "model_0": {
                # "label": f"Double Gaussian, N = {nsig1:.1f}+/-{nsig1_err:.1f}\n mu, sigma = {zfit.run(mu_sig_1):.1f}, {zfit.run(sigma_sig_1):.1f}", 
                "label": f"JohnsonSU,\n N = {nsig:.1f}+/-{nsig_err:.1f}", 
                "color":"green",
            },
        }

    if with_bkg:
        plotsList["model_1"] = {
                "label": f"Threshold poly,\n Nbkg = {result.params[Nbkg]['value']:.1f}", 
                "color":"blue",
            }

    fig, ax1, ax2 = plotFit(sum_model, data_np, plotsList, 
                    x_label="$\mu\\mu\\gamma$ mass [GeV]", 
                    nbins=nbins, linewidth=0.1, rangeX = fit_window, plot_chi2 = True,
                    logy = False)

    plt.legend(loc = 'best', fontsize = 'x-large')
    


    return data, sum_model, Nsig, Nbkg, result, fig




def eval_sweights(df, data, sum_model, Nsig, Nbkg):

    weights = compute_sweights(sum_model, data)
    df['weights_Nsig'] = weights[Nsig]
    df['weights_Nbkg'] = weights[Nbkg]

    return df


def fit_peak_bkg(data_np, 
                  fit_window, 
                  mean_vals, # mean, mean range (0.1, 0, 0.2)
                  key = '_', 
                  nbins = 50, 
                  binned_fit = True, 
                  with_bkg = True, 
                  sig_pdf_type = 'JohnsonSU', 
                  bkg_pdf_type='expo'):
    zfit.run.clear_graph_cache()

    # obs and data
    n_largest =  1.5*len(data_np)
    n_sig_init = len(data_np)*0.05
    n_bkg_init = len(data_np)*0.95
    obs_1 = zfit.Space('mass_1', limits=fit_window)
    data = zfit.data.Data.from_numpy(obs=obs_1, array=data_np)


    ## in case params are being recreated multiple times:
    ll = np.random.uniform(0,1e3)

    ## pdfs normalisation
    Nbkg = zfit.Parameter(f"Nbkg_{ll}", n_bkg_init, 0., n_largest, step_size=1)
    Nsig = zfit.Parameter(f"Nsig_{ll}", n_sig_init, 1000, n_largest, step_size=1)

    ########## sig pdfs and params
    ## Johnson SU params
    mu_sig = zfit.Parameter(f'mu_sig_1_{ll}',mean_vals[0], mean_vals[1], mean_vals[2], step_size=0.01)
    if sig_pdf_type == 'JohnsonSU':
        sigma_jh = zfit.Parameter(f'sigma_jh_{ll}',   0.0259644, 1.0e-3, 5e-2, step_size=0.1)
        nu_jh = zfit.Parameter(f'nu_{ll}', -53.7961 , -100 , 0, step_size=0.1)
        tau_jh = zfit.Parameter(f'tau_{ll}', 0.126528 , 0, 1, step_size=0.1)
        sig_pdf_2 = JohnsonSU(obs=obs_1, mu=mu_sig, sigma=sigma_jh, nu = nu_jh, tau = tau_jh)
    
    elif sig_pdf_type == 'CB':
        ## cb params
        alpha_cb = zfit.Parameter(f'alpha_cb_{ll}', 1.4 , 0, 10, step_size=0.1)
        # alpha_cb = zfit.Parameter(f'alpha_cb_{ll}', -0.5 , -10, 0, step_size=0.1)
        n_cb = zfit.Parameter(f'n_cb_{ll}', 1 , 0, 10, step_size=0.1)
        sigma_cb = zfit.Parameter(f'sigma_cb_{ll}',   3e-3, 1.0e-3, 1.2e-1, step_size=0.1)
        sig_pdf_2 = zfit.pdf.CrystalBall(obs=obs_1, mu=mu_sig, sigma=sigma_cb, n = n_cb, alpha = alpha_cb)
    sig_pdf_2_ext = sig_pdf_2.create_extended(Nsig)

    ## bkg pdf  
    if bkg_pdf_type == 'threshold':

        x0 = zfit.Parameter(f"x0_{ll}", 0.2, floating = False)
        a0 = zfit.Parameter(f"a0_{ll}", 5.60905, 1., 10., step_size=0.1)
        a1 = zfit.Parameter(f"a1_{ll}", -5.28406, -10., 10., step_size=0.1)
        alpha = zfit.Parameter(f"alpha_{ll}", 2.5, .1, 5., step_size=0.1)
        bkg_pdf = ThreshPol( obs=obs_1, x0 = x0 , a0 = a0 , a1 = a1 , alpha = alpha )

    elif bkg_pdf_type == 'expo':
        slope_exp = zfit.Parameter(f"slope_exp_{ll}", -3, -10., -0.00001, step_size=0.001) 
        bkg_pdf = zfit.pdf.Exponential(slope_exp, obs =obs_1)
    
    elif bkg_pdf_type == 'double_expo':
        slope_exp1 = zfit.Parameter(f"slope_exp_1_{ll}", -3, -10., -0.00001, step_size=0.001) 
        slope_exp2 = zfit.Parameter(f"slope_exp_2_{ll}", -1, -10., -0.00001, step_size=0.001) 

        bkg_pdf = zfit.pdf.Exponential(slope_exp, obs =obs_1)
        
    elif bkg_pdf_type == 'cheb':
        slope_cheb = zfit.Parameter(f"slope_cheb_{ll}", -0.1, -1., 1., step_size=0.001) 
        bkg_pdf = zfit.pdf.Chebyshev(obs =obs_1, coeffs = [slope_cheb])
    
    # bkg_pdf = bkg_pdf.to_binned(obs_bin)
    bkg_pdf_ext = bkg_pdf.create_extended(Nbkg)

    sum_models_list = [sig_pdf_2_ext]
    if with_bkg:
        sum_models_list.append(bkg_pdf_ext)

    if len(sum_models_list) > 1:
        sum_model  = zfit.pdf.SumPDF(sum_models_list)
    else: sum_model = sum_models_list[0]

    result = run_minimisation(sum_model, data, nbins, fit_window, binned_fit)


    nsig = result.params[Nsig]['value']    #
    # errors, _ =  result.errors()
    result.hesse(name='hesse')
    nsig_err = result.params[Nsig]['hesse']['error']

    plotsList = {
            "data":{
                "label": "Data",
                "color": "black"
            },
            "fullmodel": {
                "label": "Full fit", 
                "color":"red",
            },
            
            "model_0": {
                # "label": f"Double Gaussian, N = {nsig1:.1f}+/-{nsig1_err:.1f}\n mu, sigma = {zfit.run(mu_sig_1):.1f}, {zfit.run(sigma_sig_1):.1f}", 
                "label": f"{sig_pdf_type},\n N = {nsig:.1f}+/-{nsig_err:.1f}", 
                "color":"green",
            },
        }

    if with_bkg:
        plotsList["model_1"] = {
                "label": f"{bkg_pdf_type},\n Nbkg = {result.params[Nbkg]['value']:.1f}", 
                "color":"blue",
            }

    fig, ax1, ax2 = plotFit(sum_model, data_np, plotsList, 
                    x_label="$\mu\\mu\\gamma$ mass [GeV]", 
                    nbins=nbins, linewidth=0.1, rangeX = fit_window, plot_chi2 = True,
                    logy = False)

    plt.legend(loc = 'best', fontsize = 'x-large')
    


    return data, sum_model, Nsig, nsig_err,  Nbkg, result, fig

