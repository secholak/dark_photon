import mplhep
from uncertainties import unumpy, ufloat
from scipy.stats import chisquare
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import zfit
from zfit import z as ztf
import matplotlib.patches as mpatches
import math

import logging as lg
lg.basicConfig(format='[%(asctime)s] - %(levelname)s -  %(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=lg.INFO)
# lg.warning('is when this event was logged.')
# lg.info('info message')


isDebug = False

def chisquare_manual(observed_values,expected_values):
    test_statistic=0
    for observed, expected in zip(observed_values, expected_values):
        if expected != 0:
            test_statistic+=(float(observed)-float(expected))**2/float(expected)
    return test_statistic


def pltdist(data, bins, bounds, norm, y_label = None,  **kwargs):
    
    y, bin_edges = np.histogram(data, bins=bins, range=bounds)
    binwidth = (bounds[1] - bounds[0]) / bins
    if norm: 
        y= y/len(data)
    bin_centers = 0.5*(bin_edges[1:]+bin_edges[:-1])
    yerr = np.sqrt(y)
    if norm: 
        yerr= yerr/ len(data)
    plt.errorbar(bin_centers, y, yerr=yerr, **kwargs)
    if y_label is None:
        y_label = f"Candidates/({binwidth:.2f} [OX unit])"
    plt.ylabel(y_label)


def plotFit(
    pdf,
    data,
    plotList,
    x_label,
    y_label=None,
    nbins=50,
    plot_pull=True,
    logy=False,
    rangeX=None,
    rangeY=None,
    plot_chi2 = False,
    **kwargs):

    _range = rangeX if rangeX else pdf.space.limit1d

    if isinstance(data, tuple):
        datay, bin_edges = data[0], data[1]
    # it's a numpy array, or pd slice
    else:
        ## data --> histogram
        datay, bin_edges = np.histogram(data, bins=nbins, range=_range)

    errory = np.sqrt(datay)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    binwidth = (_range[1] - _range[0]) / nbins

    ## the pdf normalization
    try:
        integral_pdf = pdf.ext_integrate(_range)
    except zfit.util.exception.NotExtendedPDFError:
    # except AttributeError:
        integral_pdf = pdf.integrate(_range)
    if isDebug: 
        print(f"Integral of extended pdf: N = {integral_pdf}")
        print(f"Integral of  pdf: I = {pdf.integrate(_range)}")
        print("Normalization range", pdf.norm_range)


    if pdf.is_extended:
        N = zfit.run(integral_pdf)
        integral_pdf = integral_pdf / pdf.get_yield()
        if isDebug: print(f"Calculated from extended pdf: N = {N}")
    else:
        N = np.sum(datay)
        if isDebug: print(f"Calculated from datay array: N = {N}")

    norm = N * binwidth
    if isDebug: 
        print(f"Range = {_range}")
        print(f"Norm = {norm}")



    if not rangeY:
        if logy:
            if isDebug: 
                print(min(datay), max(datay))
            rangeY = (min(datay)*0.1 + 1, max(datay) * 10.0)
        else:
            rangeY = (0.01, max(datay) * 1.2)
            # pass

    yscale = "log" if logy else ""

    x = np.linspace(*_range, num=1000)

    # LHCbStyle()

    if plot_pull:
        if kwargs.get("ax1", None) is not None:
            ax1 = kwargs["ax1"]
            f = None
        else:
            f = plt.figure()
            gs = gridspec.GridSpec(2, 1, height_ratios=[5, 1], hspace=0.125)
            ax1 = plt.subplot(gs[0])
    else:
        ax1 = kwargs.get("ax1", None)
        if ax1 is None:
            ax1 = plt.gca()
        f = None

    # plot data
    if not "data" in plotList.keys():
        plotList["data"] = {"color": "black", "label": "Data"}
    datacolor = plotList["data"].get("color", "black")
    datalabel = plotList["data"].get("label", "Data")
    mplhep.histplot(
        datay,
        bins=bin_edges,
        label=datalabel,
        ax=ax1,
        histtype="errorbar",
        color=datacolor,
        markersize=4,
        yerr=True,
        elinewidth=1.5,
    )


    # plot model
    if not "fullmodel" in plotList.keys():
        plotList["fullmodel"] = {"color": "blue", "label": "Full model"}
    fmodelcolor = plotList["fullmodel"].get("color", "blue")
    fmodellabel = plotList["fullmodel"].get("label", "Full model")

    try:
        toplot = []
        toresiduals = []
        for i, (m, frac) in enumerate(zip(pdf.get_models(), pdf.fracs)):
            if m.is_extended:
                _frac = (frac / integral_pdf) * (m.ext_integrate(_range) / m.get_yield())
            else:
                _frac = (frac / integral_pdf) * m.integrate(_range)
            y = zfit.run(m.pdf(x, norm_range=_range) * _frac * norm)
            yb = zfit.run(m.pdf(bin_centers, norm_range=_range) * _frac * norm)
            toplot.append(y)
            toresiduals.append(yb)

        _ = ax1.plot(
            x, np.sum(toplot, axis=0), color=fmodelcolor, lw=2, label=fmodellabel
        )

        prop_cycle = plt.rcParams["axes.prop_cycle"]
        colors = prop_cycle.by_key()["color"]

        for i, (m, y) in enumerate(zip(pdf.get_models(), toplot)):
            if not f"model_{i}" in plotList.keys():
                plotList[f"model_{i}"] = {"color": colors[i], "label": f"model_{i}"}
            _color = plotList[f"model_{i}"].get("color", colors[i])
            _label = plotList[f"model_{i}"].get("label", f"model_{i}")
            _ = ax1.plot(x, y, ls="--", color=_color, label=_label)

        pdfy = np.sum(toresiduals, axis=0)

    except AttributeError:
        pdfy = zfit.run(pdf.pdf(bin_centers, norm_range=_range)) * norm
        _ = ax1.plot(
            x,
            zfit.run(pdf.pdf(x, norm_range=_range)) * norm,
            color=fmodelcolor,
            lw=2,
            label=fmodellabel,
        )

    if y_label is None:
        y_label = f"Candidates/({binwidth:.2f} [MeV])"

    ax1.axes.set_ylabel(y_label, ha="right", y=1)
    ax1.axes.set_xlim(_range)
    ax1.axes.set_ylim(rangeY)
    if not plot_pull:
        ax1.axes.set_xlabel(x_label, ha="right", x=1)
    else:
        ax1.set_xticklabels([])

    ax1.minorticks_on()
    ax1.legend(loc="best", fontsize=kwargs.get("fontsize", 20))

    if plot_chi2:
        nfree_params = kwargs.get("nfree_params", len(pdf.get_params(floating = True)))
        # chi2 = chisquare(datay_norm, pdfy_norm, nfree_params)[0]
        chi2 = chisquare_manual(datay, pdfy)
        ndof = nbins - 1 + nfree_params
        chi2ndof = chi2 / ndof
#         ax1.text(
#             0.8,
#             0.4,
#             r"$\chi^{2}$/ndof = " + f"{chi2ndof:.2f}" + f"\n nFree_params = {nfree_params}",
#             transform=ax1.transAxes,
#         )

        # where some data has already been plotted to ax
        handles, labels = ax1.get_legend_handles_labels()

        # manually define a new patch 
        patch = mpatches.Patch(label=r"$\chi^{2}$/ndof = " + f"{chi2ndof:.2f}" + f"\n nFree_params = {nfree_params}")

        # handles is a list, so append manual patch
        handles.append(patch) 
        # plot the legend
        ax1.legend(handles=handles, loc='best')

    ax1.legend(loc="best", fontsize=kwargs.get("fontsize", 20))
    if plot_pull:

        if kwargs.get("ax2", None) is not None:
            ax2 = kwargs["ax2"]
        else:
            ax2 = plt.subplot(gs[1])

        ax2.axes.set_ylim((-5, 5))
        ax2.axes.set_xlim(_range)
        ax2.axes.set_ylabel("Pulls")
        ax2.axes.set_xlabel(x_label, ha="right", x=1)
        ax2.plot(list(_range), [2, 2], color="red", linewidth=1.5, linestyle="-.")
        ax2.plot(list(_range), [-2, -2], color="red", linewidth=1.5, linestyle="-.")
        ax2.plot(list(_range), [0, 0], color="grey", linewidth=1.5, linestyle="-.")

        ax2.minorticks_on()
        datay = unumpy.uarray(datay, errory)
        errory = np.where(datay == 0.0, np.ones(errory.shape), errory)
        pully = (datay - pdfy) / errory
        ax2.errorbar(
            bin_centers,
            unumpy.nominal_values(pully),
            yerr=unumpy.std_devs(pully),
            fmt=".",
            ecolor="Black",
            markersize=4,
            color="Black",
            elinewidth=1.5,
        )
    else:
        ax2 = None

    try:
        f.align_ylabels()
    except (UnboundLocalError, AttributeError):
        pass

    if logy:
        # ax1.set_yscale("log", nonposy="clip")
        ax1.set_yscale("log")

    return f, ax1, ax2

from math import log

### manually profile given parameter of the nll
def profile_param(ax, param, minimum, nll, param_values = None):
    if "poi" in param.name:
        x = np.logspace(log(param.lower.numpy(), 10), log(param.upper.numpy(), 10), 100)
    else:
        x = np.linspace(param.lower.numpy(), param.upper.numpy(), num=50)
    y = []
    param.floating = False
    lg.info( f"Profiling parameter {param.name} in range of ({param.lower}, {param.upper} )")

    for val in x:
        param.set_value(val)
        y.append(nll.value())

    param.floating = True
    zfit.param.set_values(nll.get_params(), minimum)
    ax.plot(x, y, label = param.name)
    if param_values is not None:
        xx = param_values[param.name]
        y1 = min(y)
        y2 = max(y)
        ax.plot([xx, xx], [ y1, y2], color = 'red')
    # ax.legend(fontsize = 7)
    ax.legend()
    # ax.set_xlabel(param.name)
    
    if "poi" in param.name:
        ax.set_xscale('log')
        # ax.set_yscale('log')
    # plt.tight_layout()
    

    return ax


def profile_param_poi(ax, param, minimum, nll, param_values = None):


    ps = minimum.params
    es = minimum.hesse(method='minuit_hesse', name='hesse') ## symmetrical
    mean =  ps[param]['value']
    error = es[param]['error']

    lower = mean - 5*error
    if lower < param.lower.numpy(): lower = param.lower.numpy()
    upper = mean + 5*error

    x = np.linspace(lower, upper, num=100)
    y = []
    param.floating = False

    for val in x:
        param.set_value(val)
        y.append(nll.value())

    param.floating = True
    zfit.param.set_values(nll.get_params(), minimum)
    ax.plot(x, y, label = param.name)
    if param_values is not None:
        xx = param_values[param.name]
        y1 = min(y)
        y2 = max(y)
        ax.plot([xx, xx], [ y1, y2], color = 'red')
    ax.legend()
    
    if "poi" in param.name:
        ax.set_xscale('log')


    return ax


import time
### profile all free params of the fit
def profile_all_params(nll, minimum, n_rows = 2, test_set = None):
    n_params = len(nll.get_params())
    ## number of columns is a rounded up fraction
    n_cols = math.ceil(n_params/n_rows)
    fig, axes = plt.subplots(n_rows, n_cols, figsize=(5*n_params,8*n_params))

    param_values = None
    if test_set is not None:
        param_values = {}
        for p_val, p in zip(test_set, nll.get_params()):
            param_values[p.name] = p_val


    lg.info(f" {len(nll.get_params())} params to profile:")
    for param in nll.get_params():
        print(param)
    for param, ax in zip(nll.get_params(), axes.flatten()):
        # if  "poi" in param.name:
        #     profile_param_poi(ax, param, minimum, nll, param_values)

        profile_param(ax, param, minimum, nll, param_values)
        
    # plt.tight_layout()
    plt.show()
    # time.sleep(5)
